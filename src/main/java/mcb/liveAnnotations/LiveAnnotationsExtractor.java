package mcb.liveAnnotations;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import mcb.liveAnnotations.extractor.Annotation;
import mcb.liveAnnotations.extractor.AnnotationsTranslator;
import mcb.liveAnnotations.extractor.Meeting;

import javax.json.*;
import javax.json.stream.JsonParsingException;
import java.io.StringReader;
import java.util.*;

/**
 * Extract annotations from messages forwarded by other agents.
 * Provides the annotations-extractor service.
 * Delegate the storage of annotations to agents providing the annotations-store service.
 */
public class LiveAnnotationsExtractor extends Agent {
    public static final String EXTRACTOR_SERVICE = "annotations-extractor";
    public static final String STORE_SERVICE = "annotations-store";

    private Set<AID> stores = new HashSet<>(0);
    private Map<String, Meeting> meetings = new HashMap<>(0);

    private void registerToDF() {
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());

        ServiceDescription sd = new ServiceDescription();
        sd.setType(EXTRACTOR_SERVICE);
        sd.setName(getLocalName());

        dfd.addServices(sd);

        try {
            DFService.register(this, dfd);
        } catch (FIPAException e) {
            e.printStackTrace();
        }
    }

    protected void takeDown()
    {
        try {
            DFService.deregister(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findStores() {
        DFAgentDescription dfd = new DFAgentDescription();
        ServiceDescription sd  = new ServiceDescription();
        sd.setType(STORE_SERVICE);
        dfd.addServices(sd);

        DFAgentDescription[] result = new DFAgentDescription[0];
        try {
            result = DFService.search(this, dfd);
        } catch (FIPAException e) {
            e.printStackTrace();
        }

        for (DFAgentDescription dfad : result) {
            stores.add(dfad.getName());
        }

        // subscribe to new annotations-store services
        send(DFService.createSubscriptionMessage(this, getDefaultDF(), dfd, null));
        addBehaviour( new RegistrationNotification() );
    }

    class RegistrationNotification extends CyclicBehaviour
    {
        public void action()
        {
            ACLMessage msg = receive(MessageTemplate.MatchSender(getDefaultDF()));

            if (msg != null && msg.getPerformative() == ACLMessage.INFORM)
            {
                try {
                    DFAgentDescription[] dfds = DFService.decodeNotification(msg.getContent());

                    for (DFAgentDescription dfad : dfds) {
                        stores.add(dfad.getName());
                    }
                }
                catch (Exception ex) {}
            }

            block();
        }
    }

    public void setup() {
        registerToDF();
        findStores();

        addBehaviour(new CyclicBehaviour(this) {
            @Override
            public void action() {
                final ACLMessage msg = myAgent.receive(MessageTemplate.MatchPerformative(ACLMessage.INFORM));

                if (msg != null && msg.getContent() != null) {
                    if (stores.contains(msg.getSender())) {
                        // response from the store,
                        // doesn't matter for now
                    } else {
                        LiveAnnotationsExtractor.this.processMessage(msg);
                    }
                }
                block();
            }
        });
        addBehaviour(new TickerBehaviour(this, 2000) {
            @Override
            protected void onTick() {
                final ACLMessage msg = myAgent.receive(MessageTemplate.MatchPerformative(ACLMessage.INFORM));

                if (msg != null && msg.getContent() != null) {
                    if (stores.contains(msg.getSender())) {
                        // response from the store,
                        // doesn't matter for now
                    } else {
                        LiveAnnotationsExtractor.this.processMessage(msg);
                    }
                }
                block();
            }
        });
    }

    private void processMessage(ACLMessage msg) {
        try (JsonReader reader = Json.createReader(new StringReader(msg.getContent()))) {
            JsonObject message = reader.readObject();

            if (message.containsKey("topic")) {
                String topic = message.getString("topic");

                if (message.containsKey("content") && message.containsKey("agent") && message.containsKey("timestamp")) {
                    Optional<JsonObject> out_annotations = extractFrom(createOrGetMeeting(topic), message, msg.getSender().getName());
                    out_annotations.ifPresent(this::sendAnnotationsToStores);
                } else if (message.containsKey("initialContent")) {
                    createOrGetMeeting(topic).setInitState(message);
                }
            }
        } catch (JsonParsingException e) {
            e.printStackTrace();
            replyWithFailure(msg, "Could not read content, it must be in json format.");
        } catch (JsonException e) {
            e.printStackTrace();
            replyWithFailure(msg, "Could not extract data: " + e.getMessage());
        } catch (ClassCastException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            System.err.println("------------- NPE ! -------------------");
            e.printStackTrace();
        }
    }

    private void replyWithFailure(ACLMessage msg, String content) {
        ACLMessage reply = msg.createReply();
        reply.setPerformative(ACLMessage.FAILURE);
        reply.setContent(content);
        send(reply);
    }

    private void sendAnnotationsToStores(JsonObject data) {
        ACLMessage storeMessage = new ACLMessage(ACLMessage.REQUEST);
        storeMessage.setContent(data.toString());
        stores.forEach(storeMessage::addReceiver);
        send(storeMessage);
    }

    private Meeting createOrGetMeeting(String topic) {
        return meetings.computeIfAbsent(topic, _k -> new Meeting(topic));
    }

    private Optional<JsonObject> extractFrom(Meeting meeting, JsonObject data, String sender) {
        List<Annotation> annotations = meeting.extract(data);

        annotations.forEach(annotation -> {
            annotation.sender = sender;
            AnnotationsTranslator.setTranslations(annotation);
        });

        if (annotations.isEmpty()) {
            System.out.println("No annotations returned by extractors.");
            return Optional.empty();
        } else {
            System.out.println(annotations.size() + " annotations returned by extractors.");

            JsonObjectBuilder ob = Json.createObjectBuilder();
            ob.add("topic", meeting.topic);

            JsonArrayBuilder tab = Json.createArrayBuilder();
            annotations.stream().map(Annotation::toJson).forEach(tab::add);
            ob.add("annotations", tab);

            System.out.println(ob);
            return Optional.of(ob.build());
        }
    }
}
