package mcb.liveAnnotations.extractor;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class Cluster implements Element {
    private Optional<Cluster> parent;
    private Set<Element> children;
    private MultiLangString title;
    private ElementSelection selection;
    public final String uuid;

    public Cluster(String uuid, Cluster parent, LangTaggedString[] title, Set<String> selectors) {
        this.parent = Optional.ofNullable(parent);
        this.children = new HashSet<>(5);
        this.title = new MultiLangString(title);
        this.selection = new ElementSelection(selectors);
        this.uuid = uuid;
    }

    public void addChild(Element child) {
        children.add(child);
    }

    public LangTaggedString getInitialTitle() {
        return title.original();
    }

    public List<LangTaggedString> getLastTitle() {
        return title.getLastValues();
    }

    public List<LangTaggedString> getLastContent() {
        return getLastTitle();
    }

    public String getUuid() {
        return uuid;
    }

    public Optional<Cluster> getParent() {
        return parent;
    }

    public void removeParent() {
        parent = Optional.empty();
    }

    public void setParent(Cluster cluster) {
        parent = Optional.of(cluster);
    }

    public List<MultiLangString.PartialAnnotation> updateTitle(LangTaggedString... new_title) {
        return title.update(new_title);
    }

    public List<ElementSelection.PartialAnnotation> updateSelection(Set<String> new_selection) {
        return selection.set(new_selection);
    }

    public void addSelector(String selector) {
        selection.add(selector);
    }

    public void removeSelector(String selector) {
        selection.remove(selector);
    }

    public void removeChild(Element child) {
        children.remove(child);
    }
}
