package mcb.liveAnnotations.extractor;

public class AnnotationsTranslator {
    /**
     * Compute description descriptions in differents languages according to the annotation code and data.
     * Adds the description to the Annotation instance passed as argument.
     */
    public static void setTranslations(Annotation annotation) {
        AffiliateAnnotation ann_;
        switch (annotation.code) {
            case "new-postit":
                annotation.setTranslation("ENGLISH", String.format("Created a postit in %s \"%s\"", annotation.data[0].lang, annotation.data[0].str));
                annotation.setTranslation("FRENCH", String.format("Création d'un postit en %s \"%s\"", annotation.data[0].lang, annotation.data[0].str));
                annotation.setTranslation("JAPANESE", String.format("%s でのポストイット「%s」を作成しました", annotation.data[0].lang, annotation.data[0].str));
                break;
            case "new-cluster":
                annotation.setTranslation("ENGLISH", String.format("Created a cluster called \"%s\" (%s)", annotation.data[0].str, annotation.data[0].lang));
                annotation.setTranslation("FRENCH", String.format("Création d'un groupe \"%s\" (%s)", annotation.data[0].str, annotation.data[0].lang));
                break;
            case "update-postit-content":
                annotation.setTranslation("ENGLISH", String.format("Modified a postit to \"%s\" from \"%s\" (%s)", annotation.data[0].str, annotation.data[1].str, annotation.data[1].lang));
                annotation.setTranslation("FRENCH", String.format("Modification d'un postit: \"%s\", était \"%s\" (%s)", annotation.data[0].str, annotation.data[1].str, annotation.data[1].lang));
                break;
            case "update-cluster-title":
                annotation.setTranslation("ENGLISH", String.format("Modified a cluster title to \"%s\" from \"%s\" (%s)", annotation.data[0].str, annotation.data[1].str, annotation.data[1].lang));
                annotation.setTranslation("FRENCH", String.format("Modification du titre d'un groupe: \"%s\" était \"%s\" (%s)", annotation.data[0].str, annotation.data[1].str, annotation.data[1].lang));
                break;
            case "auto-translate-postit-content":
                annotation.setTranslation("ENGLISH", String.format("Translated the content of a postit to \"%s\" (%s) from \"%s\" (%s)", annotation.data[0].str, annotation.data[0].lang, annotation.data[1].str, annotation.data[1].lang));
                annotation.setTranslation("FRENCH", String.format("Traduction d'un postit: \"%s\" (%s), original: \"%s\" (%s)", annotation.data[0].str, annotation.data[0].lang, annotation.data[1].str, annotation.data[1].lang));
                break;
            case "auto-translate-cluster-title":
                annotation.setTranslation("ENGLISH", String.format("Translated the title of a cluster to \"%s\" (%s) from \"%s\" (%s)", annotation.data[0].str, annotation.data[0].lang, annotation.data[1].str, annotation.data[1].lang));
                annotation.setTranslation("FRENCH", String.format("Traduction du titre d'un groupe: \"%s\" (%s), original: \"%s\" (%s)", annotation.data[0].str, annotation.data[0].lang, annotation.data[1].str, annotation.data[1].lang));
                break;
            case "highlight-postit":
                annotation.setTranslation("ENGLISH", String.format("Highlighted the postit \"%s\"", bestStringFor(annotation.data, "ENGLISH").str));
                annotation.setTranslation("FRENCH", String.format("Sélection du postit \"%s\"", bestStringFor(annotation.data, "FRENCH").str));
                break;
            case "highlight-cluster":
                annotation.setTranslation("ENGLISH", String.format("Highlighted the cluster \"%s\"", bestStringFor(annotation.data, "ENGLISH").str));
                annotation.setTranslation("FRENCH", String.format("Sélection du groupe \"%s\"", bestStringFor(annotation.data, "FRENCH").str));
                break;
            case "un-highlight-postit":
                annotation.setTranslation("ENGLISH", String.format("Un-highlighted the postit \"%s\"", bestStringFor(annotation.data, "ENGLISH").str));
                annotation.setTranslation("FRENCH", String.format("Désélection d'un postit \"%s\"", bestStringFor(annotation.data, "FRENCH").str));
                break;
            case "un-highlight-cluster":
                annotation.setTranslation("ENGLISH", String.format("Un-highlighted the cluster \"%s\"", bestStringFor(annotation.data, "ENGLISH").str));
                annotation.setTranslation("FRENCH", String.format("Désélection d'un \"%s\"", bestStringFor(annotation.data, "FRENCH").str));
                break;
            case "delete-postit":
                annotation.setTranslation("ENGLISH", String.format("Deleted the postit \"%s\"", bestStringFor(annotation.data, "ENGLISH").str));
                annotation.setTranslation("FRENCH", String.format("Suppression du postit \"%s\"", bestStringFor(annotation.data, "FRENCH").str));
                break;
            case "delete-cluster":
                annotation.setTranslation("ENGLISH", String.format("Deleted the cluster \"%s\"", bestStringFor(annotation.data, "ENGLISH").str));
                annotation.setTranslation("FRENCH", String.format("Suppression du groupe \"%s\"", bestStringFor(annotation.data, "FRENCH").str));
                break;
            case "remove-postit-from-cluster":
                ann_ = (AffiliateAnnotation) annotation;
                annotation.setTranslation("ENGLISH", String.format("Removed the postit \"%s\" from \"%s\"", bestStringFor(ann_.data, "ENGLISH").str, bestStringFor(ann_.parent_data, "ENGLISH").str));
                annotation.setTranslation("FRENCH", String.format("Enlèvement du postit \"%s\" du groupe \"%s\"", bestStringFor(ann_.data, "FRENCH").str, bestStringFor(ann_.parent_data, "FRENCH").str));
                break;
            case "remove-cluster-from-cluster":
                ann_ = (AffiliateAnnotation) annotation;
                annotation.setTranslation("ENGLISH", String.format("Removed the cluster \"%s\" from \"%s\"", bestStringFor(ann_.data, "ENGLISH").str, bestStringFor(ann_.parent_data, "ENGLISH").str));
                annotation.setTranslation("FRENCH", String.format("Enlèvement du groupe \"%s\" du groupe \"%s\"", bestStringFor(ann_.data, "FRENCH").str, bestStringFor(ann_.parent_data, "FRENCH").str));
                break;
            case "add-postit-to-cluster":
                ann_ = (AffiliateAnnotation) annotation;
                annotation.setTranslation("ENGLISH", String.format("Moved the postit \"%s\" to \"%s\"", bestStringFor(ann_.data, "ENGLISH").str, bestStringFor(ann_.parent_data, "ENGLISH").str));
                annotation.setTranslation("FRENCH", String.format("Déplacement du postit \"%s\" dans le groupe \"%s\"", bestStringFor(ann_.data, "FRENCH").str, bestStringFor(ann_.parent_data, "FRENCH").str));
                break;
            case "add-cluster-to-cluster":
                ann_ = (AffiliateAnnotation) annotation;
                annotation.setTranslation("ENGLISH", String.format("Moved the cluster \"%s\" to \"%s\"", bestStringFor(ann_.data, "ENGLISH").str, bestStringFor(ann_.parent_data, "ENGLISH").str));
                annotation.setTranslation("FRENCH", String.format("Déplacement du groupe \"%s\" dans le groupe \"%s\"", bestStringFor(ann_.data, "FRENCH").str, bestStringFor(ann_.parent_data, "FRENCH").str));
                break;
            case "create-savepoint":
                annotation.setTranslation("ENGLISH", String.format("Created a savepoint \"%s\"", annotation.data[0].str));
                annotation.setTranslation("FRENCH", String.format("Création d'un point de sauvegarde \"%s\"", annotation.data[0].str));
                break;
            default:
                System.out.println("No translation case for annotation " + annotation.code);
                break;
        }
    }

    /**
     * Find a LangTaggedString with a given language in an array of LangTaggedString or
     * in english if there is no LangTaggedString in the requested language or
     * returns the first string in the list if english is not available.
     *
     * @param lang Requested language
     */
    private static LangTaggedString bestStringFor(LangTaggedString[] strings, String lang) {
        for (LangTaggedString string : strings) {
            if (string.lang.equals(lang)) {
                return string;
            }
        }
        if (lang.equals("ENGLISH")) {
            return strings[0];
        } else {
            return bestStringFor(strings, "ENGLISH");
        }
    }
}
