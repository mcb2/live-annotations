package mcb.liveAnnotations.extractor;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Annotation data, with description translations
 */
public class Annotation {
    public final String code;
    public final Optional<String> uuid;
    public final long timestamp; // milliseconds
    public String sender = "?";
    public final Optional<String> user_name;
    public final Map<String, String> translations = new HashMap<>(3);
    public final LangTaggedString[] data;

    public Annotation(String code, Optional<String> uuid, long timestamp, Optional<String> user_name, LangTaggedString[] data) {
        this.code = code;
        this.uuid = uuid;
        this.timestamp = timestamp;
        this.user_name = user_name;
        this.data = data;
    }

    public void setTranslation(String lang, String translation) {
        translations.put(lang, translation);
    }

    public JsonObject toJson() {
        JsonObjectBuilder b = Json.createObjectBuilder();
        b.add("code", code);
        b.add("uuid", uuid.orElse(""));
        b.add("time", timestamp);
        b.add("sender", sender);
        b.add("user_name", user_name.orElse("?"));

        JsonObjectBuilder db = Json.createObjectBuilder();
        for (Map.Entry<String, String> description : translations.entrySet()) {
            db.add(description.getKey(), description.getValue());
        }
        b.add("descriptions", db);

        return b.build();
    }
}
