package mcb.liveAnnotations.extractor;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public class Note implements Element {
    private Optional<Cluster> parent = Optional.empty();
    private MultiLangString content;
    private ElementSelection selection;
    public final String uuid;

    public Note(String uuid, Cluster parent, LangTaggedString[] content, Set<String> selectors) {
        this.parent = Optional.ofNullable(parent);
        this.content = new MultiLangString(content);
        this.selection = new ElementSelection(selectors);
        this.uuid = uuid;
    }

    public LangTaggedString getInitialContent() {
        return content.original();
    }

    public List<LangTaggedString> getLastContent() {
        return content.getLastValues();
    }

    public String getUuid() {
        return uuid;
    }

    public Optional<Cluster> getParent() {
        return parent;
    }

    public void removeParent() {
        parent = Optional.empty();
    }

    public void setParent(Cluster cluster) {
        parent = Optional.of(cluster);
    }

    public List<MultiLangString.PartialAnnotation> updateContent(LangTaggedString... new_content) {
        return content.update(new_content);
    }

    public List<ElementSelection.PartialAnnotation> updateSelection(Set<String> new_selection) {
        return selection.set(new_selection);
    }

    public void addSelector(String selector) {
        selection.add(selector);
    }

    public void removeSelector(String selector) {
        selection.remove(selector);
    }
}
