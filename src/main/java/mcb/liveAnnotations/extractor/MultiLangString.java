package mcb.liveAnnotations.extractor;

import java.util.*;

public class MultiLangString {
    static class PartialAnnotation {
        public final String code;
        public final LangTaggedString old_str;
        public final LangTaggedString new_str;

        PartialAnnotation(String code, LangTaggedString old_str, LangTaggedString new_str) {
            this.code = code;
            this.old_str = old_str;
            this.new_str = new_str;
        }
    }

    private final ArrayList<LangTaggedString> strings = new ArrayList<>();

    public MultiLangString(LangTaggedString... content) {
        Collections.addAll(strings, content);
    }

    public Optional<LangTaggedString> getLastForLang(String lang) {
        for (int i = strings.size() - 1; i >= 0; i--) {
            if (strings.get(i).isIn(lang)) {
                return Optional.of(strings.get(i));
            }
        }
        return Optional.empty();
    }

    protected LangTaggedString add(String lang, String str) {
        LangTaggedString n = new LangTaggedString(lang, str);
        strings.add(n);
        return n;
    }

    protected LangTaggedString original() {
        return strings.get(0);
    }

    public List<PartialAnnotation> update(LangTaggedString[] content) {
        List<PartialAnnotation> result = new ArrayList<>(3);

        for (LangTaggedString x : content) {
            Optional<LangTaggedString> last = getLastForLang(x.lang);
            if (last.isPresent()) {
                if (!last.get().str.equals(x.str)) {
                    LangTaggedString n = add(x.lang, x.str);
                    result.add(new PartialAnnotation("update", last.get(), n));
                }
            } else {
                LangTaggedString n = add(x.lang, x.str);
                result.add(new PartialAnnotation("auto-translate", original(), n));
            }
        }

        return result;
    }

    /**
     * Returns the last string available for each language
     */
    public List<LangTaggedString> getLastValues() {
        Set<String> visitedLangs = new HashSet<>(3);

        ListIterator<LangTaggedString> li = strings.listIterator(strings.size());
        List<LangTaggedString> result = new ArrayList<>(3);

        while (li.hasPrevious()) {
            LangTaggedString s = li.previous();
            if (!visitedLangs.contains(s.lang)) {
                result.add(s);
                visitedLangs.add(s.lang);
            }
        }

        return result;
    }
}
