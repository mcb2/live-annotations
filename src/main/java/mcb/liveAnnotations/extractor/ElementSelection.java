package mcb.liveAnnotations.extractor;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ElementSelection {
    static class PartialAnnotation {
        public final String code;
        public final String name;

        PartialAnnotation(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    private Set<String> selection = new HashSet<>(2);

    public ElementSelection(Set<String> selection) {
        this.selection = selection; // java sets are mutable so we may need to clone it here if some other place in
        // the code ends up keeping a reference to the selection set
    }

    public Stream<PartialAnnotation> selectionDiff(Set<String> ref, Set<String> other, String message) {
        return ref.stream().filter(selector -> !other.contains(selector))
                .map(selector -> new PartialAnnotation(message, selector));
    }

    public List<PartialAnnotation> set(Set<String> new_selection) {
        List<PartialAnnotation> result = Stream.concat(
                selectionDiff(selection, new_selection, "un-highlight"),
                selectionDiff(new_selection, selection, "highlight")
        ).collect(Collectors.toList());

        selection = new_selection;

        return result;
    }

    public void add(String selector) {
        selection.add(selector);
    }

    public void remove(String selector) {
        selection.remove(selector);
    }
}
