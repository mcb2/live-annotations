package mcb.liveAnnotations.extractor;

import java.util.Optional;

public class AffiliateAnnotation extends Annotation {
    public final LangTaggedString[] parent_data;

    public AffiliateAnnotation(String code, Optional<String> uuid, long timestamp, Optional<String> user_name, LangTaggedString[] data, LangTaggedString[] parent_data) {
        super(code, uuid, timestamp, user_name, data);
        this.parent_data = parent_data;
    }
}
