package mcb.liveAnnotations.extractor;

import java.util.List;
import java.util.Optional;

public interface Element {
    public String getUuid();
    public Optional<Cluster> getParent();
    public void removeParent();
    public void setParent(Cluster cluster);
    public List<LangTaggedString> getLastContent();
}
