package mcb.liveAnnotations.extractor;

/**
* Created by gakusei on 6/2/2015.
*/
class LangTaggedString {
    public final String lang;
    public final String str;

    LangTaggedString(String lang, String str) {
        this.lang = lang;
        this.str = str;
    }

    public boolean isIn(String lang) {
        return this.lang.equals(lang);
    }

    @Override
    public String toString() {
        return "<LTS:" + lang + "> " + str;
    }
}
