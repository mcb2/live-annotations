package mcb.liveAnnotations.extractor;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Represent a meeting
 * Store re-constructed meeting state
 * Contains methods that
 */
public class Meeting {
    public final String topic;
    public final Map<String, Element> elements;

    public Meeting(String topic) {
        assert topic != null;

        this.topic = topic;
        this.elements = new HashMap<>(30);
    }

    /**
     * The meeting agent is supposed to send the complete meeting state when it discovers the annotations agent
     * or when a meeting starts
     */
    public void setInitState(JsonObject data) {
        elements.clear();

        data.getJsonObject("initialContent").getJsonArray("children").forEach(c ->
                this.setObject((JsonObject) c, null));
    }

    public void setObject(JsonObject object, Cluster parent) {
        switch (object.getString("type")) {
            case "postit":
                Note note = createNote(object, parent);
                elements.put(note.uuid, note);

                System.out.println("note:    " + note.getUuid() + (parent == null ? "" : " (" + parent.getUuid() + ")"));

                break;
            case "cluster":
                Cluster cluster = createCluster(object, parent);
                elements.put(cluster.uuid, cluster);

                System.out.println("cluster: " + cluster.getUuid() + (parent == null ? "" : " (" + parent.getUuid() + ")"));

                object.getJsonArray("children").forEach(c -> this.setObject((JsonObject) c, cluster));
        }
    }

    public List<Annotation> extract(JsonObject data) {
        // maybe we should store extractors functions in a map
        // Map<String, BiFunction<Meeting, JsonObject, List<Annotation>>> extractors;
        // and ad them at Meeting construction, but we'll see that later
        switch (data.getJsonObject("content").getString("action")) {
            case "update":
                return actionUpdate(data);
            case "create":
                return actionCreate(data);
            case "select":
                return actionSelect(data);
            case "unselect":
                return actionUnselect(data);
            case "new-topic":
                return Collections.EMPTY_LIST;
            case "delete":
                return actionDelete(data);
            case "create-savepoint":
                return actionCreateSavepoint(data);
            case "affiliate-object":
                return actionAffiliateObject(data);
            default:
                return Collections.EMPTY_LIST;
        }
    }

    public List<Annotation> actionCreate(JsonObject data) {
        switch (data.getJsonObject("content").getJsonObject("object").getString("type")) {
            case "postit":
                return actionCreateNote(data);
            case "cluster":
                return actionCreateCluster(data);
            default:
                return Collections.EMPTY_LIST;
        }
    }

    LangTaggedString[] getContent(JsonObject object) {
        return object.getJsonObject("content").getJsonObject("content").entrySet().stream()
                .map(entry -> new LangTaggedString(entry.getKey(), ((JsonString)entry.getValue()).getString()))
                .toArray(LangTaggedString[]::new);
    }

    public List<Annotation> actionCreateNote(JsonObject data) {
        JsonObject object = data.getJsonObject("content").getJsonObject("object");

        Note new_note = createNote(object);
        elements.put(new_note.uuid, new_note);

        return Collections.singletonList(new Annotation("new-postit", Optional.of(new_note.uuid),
                data.getJsonNumber("timestamp").longValue(),
                Optional.of(object.getJsonObject("author").getString("name")),
                new LangTaggedString[]{new_note.getInitialContent()}));
    }

    private Note createNote(JsonObject object, Cluster parent) {
        return new Note(object.getString("id"), parent, getContent(object), getSelectors(object));
    }

    private Note createNote(JsonObject object) {
        return createNote(object, null);
    }

    public List<Annotation> actionCreateCluster(JsonObject data) {
        JsonObject object = data.getJsonObject("content").getJsonObject("object");

        Cluster new_cluster = createCluster(object);
        elements.put(new_cluster.uuid, new_cluster);

        return Collections.singletonList(new Annotation("new-cluster", Optional.of(new_cluster.uuid),
                data.getJsonNumber("timestamp").longValue(),
                Optional.of(object.getJsonObject("author").getString("name")),
                new LangTaggedString[]{new_cluster.getInitialTitle()}));
    }

    private Cluster createCluster(JsonObject object, Cluster parent) {
        Cluster c = new Cluster(object.getString("id"), parent, getContent(object), getSelectors(object));
        if (parent != null) {
            parent.addChild(c);
        }
        return c;
    }

    private Cluster createCluster(JsonObject object) {
        return createCluster(object, null);
    }

    public List<Annotation> actionDelete(JsonObject data) {
        String uuid = data.getJsonObject("content").getString("objectId");
        Element elt = elements.get(uuid);

        if (elt != null) {
            elements.remove(uuid);

            if (elt instanceof Note) {
                Note note = (Note) elt;

                return Collections.singletonList(new Annotation("delete-postit", Optional.of(note.uuid),
                        data.getJsonNumber("timestamp").longValue(), Optional.empty(),
                        note.getLastContent().stream().toArray(LangTaggedString[]::new)));
            } else if (elt instanceof Cluster) {
                Cluster cluster = (Cluster) elt;

                return Collections.singletonList(new Annotation("delete-cluster", Optional.of(cluster.uuid),
                        data.getJsonNumber("timestamp").longValue(), Optional.empty(),
                        cluster.getLastTitle().stream().toArray(LangTaggedString[]::new)));
            } else {
                return Collections.EMPTY_LIST;
            }
        } else {
            return Collections.EMPTY_LIST;
        }
    }

    public List<Annotation> actionUpdate(JsonObject data) {
        if (data.getJsonObject("content").isNull("object")) {
            return Collections.EMPTY_LIST;
        }

        switch (data.getJsonObject("content").getJsonObject("object").getString("type")) {
            case "postit":
                return actionUpdateNote(data);
            case "cluster":
                return actionUpdateCluster(data);
            default:
                return Collections.EMPTY_LIST;
        }
    }

    private List<Annotation> actionUpdateNote(JsonObject data) {
        JsonObject object = data.getJsonObject("content").getJsonObject("object");
        Element elt = elements.get(object.getString("id"));

        if (elt != null && elt instanceof Note) {
            Note note = (Note) elt;

            return Stream.concat(
                    note.updateContent(getContent(object)).stream().map((pannot) -> {
                        return new Annotation(pannot.code + "-postit-content", Optional.of(note.uuid),
                                data.getJsonNumber("timestamp").longValue(), Optional.empty(),
                                new LangTaggedString[]{pannot.new_str, pannot.old_str});
                    }),
                    note.updateSelection(getSelectors(object)).stream().map((pannot) -> {
                        return new Annotation(pannot.code + "-postit", Optional.of(note.uuid),
                                data.getJsonNumber("timestamp").longValue(), Optional.of(pannot.name),
                                note.getLastContent().stream().toArray(LangTaggedString[]::new));
                    })).collect(Collectors.toList());
        } else {
            return Collections.EMPTY_LIST;
        }
    }

    private List<Annotation> actionUpdateCluster(JsonObject data) {
        JsonObject object = data.getJsonObject("content").getJsonObject("object");
        Element elt = elements.get(object.getString("id"));

        if (elt != null && elt instanceof Cluster) {
            Cluster cluster = (Cluster) elt;

            return Stream.concat(
                    cluster.updateTitle(getContent(object)).stream().map((pannot) -> {
                        return new Annotation(pannot.code + "-cluster-title", Optional.of(cluster.uuid),
                                data.getJsonNumber("timestamp").longValue(), Optional.empty(),
                                new LangTaggedString[]{pannot.new_str, pannot.old_str});
                    }),
                    cluster.updateSelection(getSelectors(object)).stream().map((pannot) -> {
                        return new Annotation(pannot.code + "-cluster", Optional.of(cluster.uuid),
                                data.getJsonNumber("timestamp").longValue(), Optional.of(pannot.name),
                                cluster.getLastTitle().stream().toArray(LangTaggedString[]::new));
                    })).collect(Collectors.toList());
        } else {
            return Collections.EMPTY_LIST;
        }
    }

    private List<Annotation> actionCreateSavepoint(JsonObject data) {
        return Collections.singletonList(new Annotation("create-savepoint", Optional.of(this.topic),
                data.getJsonNumber("timestamp").longValue(), Optional.empty(),
                new LangTaggedString[] {new LangTaggedString("", data.getJsonObject("content").getString("name"))})); // this is not good...
    }

    private Set<String> getSelectors(JsonObject object) {
        return object.getJsonArray("selectedBy").stream()
                .map(selector -> ((JsonObject) selector).getString("name"))
                .collect(Collectors.toSet());
    }

    private List<Annotation> actionSelect(JsonObject data) {
        String selector = data.getJsonObject("content").getJsonObject("user").getString("name");
        JsonArray objects = data.getJsonObject("content").getJsonArray("objectIds");
        List<Annotation> result = new ArrayList<>();

        for(int i= 0; i < objects.size(); i++) {
            Element elt = elements.get(objects.getString(i));

            if (elt instanceof Note) {
                Note note = (Note) elt;

                note.addSelector(selector);
                result.add(new Annotation("highlight-postit", Optional.of(note.uuid),
                        data.getJsonNumber("timestamp").longValue(), Optional.of(selector),
                        note.getLastContent().stream().toArray(LangTaggedString[]::new)));
            } else if (elt instanceof Cluster) {
                Cluster cluster = (Cluster) elt;

                cluster.addSelector(selector);
                result.add(new Annotation("highlight-cluster", Optional.of(cluster.uuid),
                        data.getJsonNumber("timestamp").longValue(), Optional.of(selector),
                        cluster.getLastTitle().stream().toArray(LangTaggedString[]::new)));
            }
        }

        return result;
    }

    private List<Annotation> actionUnselect(JsonObject data) {
        String selector = data.getJsonObject("content").getJsonObject("user").getString("name");
        JsonArray objects = data.getJsonObject("content").getJsonArray("objectIds");
        List<Annotation> result = new ArrayList<>();

        for(int i= 0; i < objects.size(); i++) {
            Element elt = elements.get(objects.getString(i));

            if (elt instanceof Note) {
                Note note = (Note) elt;

                note.removeSelector(selector);
                result.add(new Annotation("un-highlight-postit", Optional.of(note.uuid),
                        data.getJsonNumber("timestamp").longValue(), Optional.of(selector),
                        note.getLastContent().stream().toArray(LangTaggedString[]::new)));
            } else if (elt instanceof Cluster) {
                Cluster cluster = (Cluster) elt;

                cluster.removeSelector(selector);
                result.add(new Annotation("un-highlight-cluster", Optional.of(cluster.uuid),
                        data.getJsonNumber("timestamp").longValue(), Optional.of(selector),
                        cluster.getLastTitle().stream().toArray(LangTaggedString[]::new)));
            }
        }

        return result;
    }

    private List<Annotation> actionAffiliateObject(JsonObject data) {
        JsonObject content = data.getJsonObject("content");
        String childId = content.getString("childId");

        Element child = elements.get(childId);

        String type;
        if (child instanceof Note) {
            type = "postit";
        } else if (child instanceof Cluster) {
            type = "cluster";
        } else {
            return Collections.EMPTY_LIST;
        }

        if (content.isNull("parentId")) {
            Optional<Cluster> oldParentO = child.getParent();

            if (oldParentO.isPresent()) {
                Cluster oldParent = oldParentO.get();

                child.removeParent();
                oldParent.removeChild(child);

                return Collections.singletonList(new AffiliateAnnotation("remove-" + type + "-from-cluster", Optional.of(childId),
                        data.getJsonNumber("timestamp").longValue(), Optional.empty(),
                        child.getLastContent().stream().toArray(LangTaggedString[]::new),
                        oldParent.getLastTitle().stream().toArray(LangTaggedString[]::new)));
            } else {
                return Collections.EMPTY_LIST;
            }
        } else {
            // todo, there is one more case where the element already has a parent cluster and we move it to another one
            String parentId = content.getString("parentId");
            Cluster parent = (Cluster) elements.get(parentId);

            parent.addChild(child);
            child.setParent(parent);

            return Collections.singletonList(new AffiliateAnnotation("add-" + type + "-to-cluster", Optional.of(childId),
                    data.getJsonNumber("timestamp").longValue(), Optional.empty(),
                    child.getLastContent().stream().toArray(LangTaggedString[]::new),
                    parent.getLastTitle().stream().toArray(LangTaggedString[]::new)));
        }
    }
}