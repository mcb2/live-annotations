package mcb.liveAnnotations;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import javax.json.*;
import javax.json.stream.JsonParsingException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.*;

/**
 * Save annotations to a mysql database.
 * Provides the annotations-store service.
 */
public class AnnotationMysqlStore extends Agent {
    public static final String STORE_SERVICE = "annotations-store";

    private String mysql_server;
    private String mysql_port;
    private String mysql_user;
    private String mysql_password;
    private String mysql_database_name;

    private Connection connection;

    private void extractConfigsOrFail(Object[] args) {
        if (args.length < 5 || hasNulls(args)) {
            throw new IllegalArgumentException("You must specify the mysql server ip, port, user, password and database name as arguments, in this order.");
        }

        mysql_server = args[0].toString();
        mysql_port = args[1].toString();
        mysql_user = args[2].toString();
        mysql_password = args[3].toString();
        mysql_database_name = args[4].toString();
    }

    public boolean hasNulls(Object[] args) {
        for (Object x : args)
            if (x == null)
                return true;

        return false;
    }

    private void registerToDF() {
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());

        ServiceDescription sd = new ServiceDescription();
        sd.setType(STORE_SERVICE);
        sd.setName(getLocalName());

        dfd.addServices(sd);

        try {
            DFService.register(this, dfd);
        } catch (FIPAException e) {
            e.printStackTrace();
        }
    }

    protected void takeDown()
    {
        try {
            connection.close();
            DFService.deregister(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void connectToDatabase() throws UnsupportedEncodingException, SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        // load driver class
        Class.forName("com.mysql.jdbc.Driver").newInstance();

        connection = DriverManager.getConnection(connectionUri());
    }

    private String connectionUri() throws UnsupportedEncodingException {
        String port = mysql_port.length() > 0 ? (":" + mysql_port) : "";
        return String.format("jdbc:mysql://%s%s/%s?user=%s&password=%s&autoReconnect=true&useUnicode=true&characterEncoding=UTF-8&autoReconnect=true",
                mysql_server, port, mysql_database_name,
                URLEncoder.encode(mysql_user, "UTF-8"), URLEncoder.encode(mysql_password, "UTF-8"));
    }

    private void createAnnotationsTable() throws SQLException {
        connection.createStatement().execute("create table if not exists mcb_annotations (" +
                "    id int auto_increment primary key," +
                "    topic varchar(40) not null," +
                "    uuid varchar(40) not null," +
                "    code varchar(30) not null," +
                "    time int not null," +
                "    sender varchar(20)," +
                "    user_name varchar(50)," +
                "    data text not null," +
                "    lang varchar(3) not null)");
    }

    public void setup() {
        extractConfigsOrFail(getArguments());

        try {
            connectToDatabase();
            createAnnotationsTable();
        } catch (Exception e) {
            e.printStackTrace();
            doDelete();
            return;
        }

        registerToDF();

        addBehaviour(new CyclicBehaviour(this) {
            @Override
            public void action() {
                final ACLMessage msg = myAgent.receive(MessageTemplate.MatchPerformative(ACLMessage.REQUEST));

                if (msg != null && msg.getContent() != null) {
                    try (JsonReader reader = Json.createReader(new StringReader(msg.getContent()))) {
                        JsonObject message = reader.readObject();
                        int saved_annotations = saveAnnotations(message.getString("topic"), message.getJsonArray("annotations"));

                        ACLMessage reply = msg.createReply();
                        reply.setPerformative(ACLMessage.INFORM);
                        reply.setContent(String.valueOf(saved_annotations));
                        send(reply);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    } catch (JsonParsingException e) {
                        e.printStackTrace();
                        ACLMessage reply = msg.createReply();
                        reply.setPerformative(ACLMessage.FAILURE);
                        reply.setContent("Could not read content, it must be in json format.");
                        send(reply);
                    } catch (JsonException e) {
                        e.printStackTrace();
                    } catch (ClassCastException e) {
                        e.printStackTrace();
                    }
                }
                block();
            }
        });
    }

    private int saveAnnotations(String topic, JsonArray annotations) throws SQLException {
        int saved_messages = 0;
        PreparedStatement insert = connection.prepareStatement("insert into mcb_annotations (topic, uuid, code, time, sender, user_name, data, lang) values (?,?,?,?,?,?,?,?)");

        for(int i = 0; i < annotations.size(); i++) {
            JsonObject annotation = annotations.getJsonObject(i);
            insert.setString(1, topic);
            insert.setString(2, annotation.getString("uuid"));
            insert.setString(3, annotation.getString("code"));
            insert.setString(4, annotation.getJsonNumber("time").toString());
            insert.setString(5, annotation.getString("sender"));
            insert.setString(6, annotation.getString("user_name"));

            JsonObject descriptions = annotation.getJsonObject("descriptions");
            for(String lang : descriptions.keySet()) {
                String description = descriptions.getString(lang);
                System.out.println("*"+lang+": "+description);

                insert.setString(7, description);
                insert.setString(8, lang);

                insert.executeUpdate();
                saved_messages++;
            }
        }

        return saved_messages;
    }

}
