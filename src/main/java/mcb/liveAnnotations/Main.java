package mcb.liveAnnotations;

import jade.core.*;
import jade.core.Runtime;
import jade.wrapper.*;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("You must specify the config file path as first argument.");
        }

        try {
            Profile profile = new ProfileImpl(args[0]);

            jade.core.Runtime rt = Runtime.instance();

            final jade.wrapper.AgentContainer ac = profile.isMain() ? rt.createMainContainer(profile)
                    : rt.createAgentContainer(profile);

            String random_id = genRandomId();

            ac.createNewAgent("annotations-mysql-" + random_id, AnnotationMysqlStore.class.getCanonicalName(), new Object[] {
                    profile.getParameter("mysql-server", null),
                    profile.getParameter("mysql-port", null),
                    profile.getParameter("mysql-user", null),
                    profile.getParameter("mysql-password", null),
                    profile.getParameter("mysql-database-name", null)
            }).start();
            ac.createNewAgent("live-annotations-" + random_id, LiveAnnotationsExtractor.class.getCanonicalName(), null).start();

            java.lang.Runtime.getRuntime().addShutdownHook(
                    new Thread(() -> {
                        try {
                            ac.kill();
                        } catch (StaleProxyException e) {
                            e.printStackTrace();
                        }
                    }));
        } catch (ProfileException e) {
            e.printStackTrace();
        } catch (StaleProxyException e) {
            e.printStackTrace();
        }
    }

    public static String genRandomId() {
        Random rgen = new Random();
        return String.valueOf(Math.abs(rgen.nextInt()));
    }
}
